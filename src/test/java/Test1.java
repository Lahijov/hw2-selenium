import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.Point;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Test1 {

    @Test
    void locators1() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        String expected = "Google";
        Actions actions = new Actions(webDriver);
        try {
            webDriver.get("https://www.google.com");

            WebElement googleTextElement = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input"));

            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            googleTextElement.sendKeys("Selenium");

            WebElement xpath = webDriver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]"));
            xpath.click();
            WebDriverWait checMail = new WebDriverWait(webDriver, 10);
            checMail.until(ExpectedConditions.visibilityOfElementLocated(By.className("hide-focus-ring")));
            WebElement classname = webDriver.findElement(By.className("hide-focus-ring"));
            classname.click();
            checMail.until(ExpectedConditions.visibilityOfElementLocated(By.id("REsRA")));

            WebElement id = webDriver.findElement(By.id("REsRA"));
            id.clear();

            id.sendKeys("Google");
            WebElement xpath2 = webDriver.findElement(By.xpath("//*[@id=\"BIqFsb\"]"));
            xpath2.click();
            WebElement xpath3 = webDriver.findElement(By.xpath("//*[@id=\"REsRA\"]"));


            Assertions.assertEquals(expected, xpath3.getAttribute("value"));


        } finally {
            webDriver.close();
        }

    }

    @Test
    void newWindow_button2() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        String expected = new String("Selenium WebDriver");
        try {

            driver.manage().window().maximize();

            driver.get("    https://www.w3schools.com/");
            WebElement tryy = driver.findElement(By.xpath("//*[@id=\"main\"]/div[1]/div/div[2]/div/a"));
            tryy.click();
            Set<String> li = driver.getWindowHandles();
            Iterator<String> i = li.iterator();
            String first = i.next();
            String next = i.next();

            WebDriverWait webSelenium = new WebDriverWait(driver, 10);
            driver.switchTo().window(next);

            webSelenium.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div/button")));
            Assertions.assertTrue(driver.findElement(By.xpath("/html/body/div[5]/div/button")).isDisplayed());
            driver.close();
            driver.switchTo().window(first);


        } finally {
            driver.close();
        }
    }

    @Test
    void newWindow_tab2() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        String expected = new String("Selenium WebDriver");
        try {

            driver.manage().window().maximize();

            driver.get("http://yahoo.com");
            ((JavascriptExecutor) driver).executeScript("window.open()");
            ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
            driver.switchTo().window(tabs.get(1));
            driver.get("http://google.com");
            WebElement googleTextElement = driver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input"));

            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            googleTextElement.sendKeys("Selenium");

            WebElement xpath = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]"));
            xpath.click();

            WebElement xpath2 = driver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div/div[1]/a/h3"));
            System.out.println(xpath2.getText());
            Assertions.assertEquals(expected, xpath2.getText());
            driver.close();
            driver.switchTo().window(tabs.get(0));


        } finally {
            driver.close();
        }
    }


    @Test
    void switchTo3() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        try {
            webDriver.get("http://demo.automationtesting.in/Alerts.html");
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement xpath = webDriver.findElement(By.xpath("//*[@id=\"OKTab\"]/button"));
            xpath.click();

            Alert alert = webDriver.switchTo().alert();
            alert.accept();
            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            Assertions.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"OKTab\"]/button")).isDisplayed());


        } finally {
            webDriver.close();
        }
    }

    @Test
    void iFrame4() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();


        try {
            webDriver.get("https://www.w3schools.com/html/tryit.asp?filename=tryhtml_form_submit");
            webDriver.manage().window().maximize();
            webDriver.switchTo().frame(webDriver.findElement(By.xpath("//*[@id=\"iframeResult\"]")));

            WebElement name = webDriver.findElement(By.xpath("//*[@id=\"fname\"]"));
            name.clear();
            name.sendKeys("Rafig");
            WebElement lastname = webDriver.findElement(By.xpath("//*[@id=\"lname\"]"));

            lastname.clear();
            lastname.sendKeys("Lahijov");
            WebElement submit = webDriver.findElement(By.xpath("/html/body/form/input[3]"));
            submit.click();

            WebDriverWait checMail = new WebDriverWait(webDriver, 10);
            checMail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]")));
            WebElement last = webDriver.findElement(By.xpath("/html/body/div[1]"));
            Assertions.assertTrue(last.getText().contains("Rafig"));
            Assertions.assertTrue(last.getText().contains("Lahijov"));


        } finally {
            webDriver.close();
        }
    }


    @Test
    void Select5() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        String expected = "Åland Islands";
        try {
            webDriver.get("https://www.globalsqa.com/demo-site/select-dropdown-menu/");
            WebElement dropdown = webDriver.findElement(By.xpath("//*[@id=\"post-2646\"]/div[2]/div/div/div/p/select"));
            Select select = new Select(dropdown);
            select.selectByIndex(1);
            System.out.println(select.getFirstSelectedOption().getText());
            Assertions.assertEquals(expected, select.getFirstSelectedOption().getText());

        } finally {
            webDriver.close();
        }
    }

    @Test
    void ordinaryDropdown6() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        String expected = "RU";
        try {
            webDriver.get("https://hesab.az/#/");
            WebDriverWait webSelenium = new WebDriverWait(webDriver, 10);
            WebElement dropw = webDriver.findElement(By.xpath("/html/body/app-root/app-landing/header/div/ul/li[2]/div"));
            dropw.click();


            webSelenium.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/app-landing/header/div/ul/li[2]/div/ul/li")));
            List<WebElement> dropdown = webDriver.findElements(By.xpath("/html/body/app-root/app-landing/header/div/ul/li[2]/div/ul/li"));
            dropdown.get(1).click();

            WebElement currentStatus = webDriver.findElement(By.xpath("/html/body/app-root/app-landing/header/div/ul/li[2]/div"));
            System.out.println(currentStatus.getText());


            Assertions.assertEquals(expected, currentStatus.getText());


        } finally {
            webDriver.close();
        }
    }

    @Test
    void alarmPopUp7() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        try {
            webDriver.get("https://www.seleniumeasy.com/test/window-popup-modal-demo.html");
            WebDriverWait webSelenium = new WebDriverWait(webDriver, 10);
            webSelenium.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div/div[2]/div[1]/a")));


            WebElement webElement = webDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div/div[2]/div[1]/a"));
            webElement.click();
            Set<String> popups = webDriver.getWindowHandles();
            Iterator<String> i = popups.iterator();
            String first = i.next();
            String pop = i.next();
            webDriver.switchTo().window(pop).close();
            webDriver.switchTo().window(first);
            Assertions.assertTrue(webDriver.getWindowHandles().size() == 1);


        } finally {
            webDriver.close();
        }
    }

    @Test
    void actions8() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        Actions actions = new Actions(webDriver);
        try {
            webDriver.get("https://www.johnlewis.com/");
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement webElement = webDriver.findElement(By.xpath("//*[@id=\"pecr-cookie-banner-wrapper\"]/div/div[1]/div/div[2]/button[1]"));
            webElement.click();
            actions.moveToElement(webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/header/nav/div[2]/ul/li[1]/a"))).perform();
            List<WebElement> list = webDriver.findElements(By.xpath("//*[@id=\"root\"]/div/header/nav/div[2]/ul/li[1]/div/ul/li[1]/div/ul/li"));
            actions.moveToElement(list.get(0)).click().perform();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            WebElement findOutMore = webDriver.findElement(By.xpath("//*[@id=\"content-root-content-renderer-category-9500190102\"]/div/div[3]/div/div/div/div/div[1]/div/div/div/div/div[7]/div/p/b/a"));
            actions.moveToElement((findOutMore)).doubleClick().perform();
            WebElement hangOn = webDriver.findElement(By.xpath("//*[@id=\"content-root-content-renderer-customer-help-delivery-information-click-and-collect-top\"]/div/div[1]/div/div/div[2]/div/span/span"));
            Assertions.assertTrue(hangOn.isDisplayed());


        } finally {
            webDriver.close();
        }
    }

    @Test
    void driverManage9() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        try {
            webDriver.get("https://www.google.com/");
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            webDriver.manage().window().setPosition(new Point((32), (42)));
            System.out.println(webDriver.manage().window().getPosition().toString());

            System.out.println("Height of window is " + webDriver.manage().window().getSize().getHeight());
            System.out.println("Width of window is " + webDriver.manage().window().getSize().getWidth());

            System.out.println(webDriver.manage().window().getPosition().x);

            Assertions.assertTrue(webDriver.manage().window().getPosition().getX() == 32);


        } finally {
            webDriver.close();
        }
    }

    @Test
    void uplodad10() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        Actions actions = new Actions(webDriver);
        String expected = "Test1.txt";
        File test1 = new File("src\\test\\resources\\Test1.txt");
        try {
            //UPLOADING FILE
            webDriver.get("https://www.monsterindia.com/seeker/registration");
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            WebElement file = webDriver.findElement(By.xpath("//input[@id='file-upload']"));
            file.sendKeys(test1.getCanonicalPath());

            WebElement text = webDriver.findElement(By.xpath("//*[@id=\"registerThemeDefault\"]/div/div[3]/div/div/div/form/div[1]/div[2]/div/div/div[1]/div/div[2]/p"));
            Assertions.assertEquals(expected, text.getText());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();

        }
    }


    @Test
    void Downland10() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        Actions actions = new Actions(webDriver);
        String expected = "text.txt";
        try {
            webDriver.get("https://chromedriver.storage.googleapis.com/index.html?path=79.0.3945.36/");
            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebDriverWait webSelenium = new WebDriverWait(webDriver, 10);
            webSelenium.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/table/tbody/tr[6]/td[2]/a")));
            WebElement btnDownload = webDriver.findElement(By.xpath("/html/body/table/tbody/tr[6]/td[2]/a"));
            btnDownload.click();
            Thread.sleep(30000);//Waiting for dowlanding


        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();

        }
    }
}